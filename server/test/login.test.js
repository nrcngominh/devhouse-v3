import supertest from "supertest";
import sinon from "sinon";
import app from "app";
import account from "models/account";

const request = supertest(app);

describe("POST /api/login", () => {
  afterEach(() => {
    sinon.restore();
  });

  it("logins successfully for valid input", async () => {
    sinon.stub(account, "findOne").resolves(true);

    const response = await request.post("/api/login").send({
      email: "dummy@gmail.com",
      password: "dummy",
    });

    expect(response.status).toBe(200);
  });

  it("logins failed for valid input", async () => {
    sinon.stub(account, "findOne").resolves(null);

    const response = await request.post("/api/login").send({
      email: "dummy@gmail.com",
      password: "dummy",
    });

    expect(response.status).toBe(401);
  });
});
