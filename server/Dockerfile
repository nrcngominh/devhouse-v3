# Base

FROM node:14-alpine AS base

USER node
RUN mkdir /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node ./package.json ./yarn.lock ./
RUN yarn install --frozen-lockfile

COPY . .

RUN yarn build

# Production

FROM node:14-alpine AS production

ENV NODE_ENV production

USER node
RUN mkdir /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node ./package.json ./yarn.lock ./
RUN yarn install --frozen-lockfile

COPY --chown=node:node --from=base /home/node/app/dist ./dist

CMD [ "node", "dist/index.js"]

# Dev only

FROM node:14 AS dev

USER node
RUN mkdir /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node ./package.json ./yarn.lock ./
RUN yarn install --frozen-lockfile
