import accountRepo from "repositories/account";

const loginAdmin = (email, password) => {
  return accountRepo.findByEmailPassword(email, password);
};

export default {
  loginAdmin,
};
