import accountModel from "models/account";

const findByEmailPassword = (email, password) => {
  return accountModel.findOne({
    email,
    password,
    type: "admin",
  });
};

export default {
  findByEmailPassword,
};
