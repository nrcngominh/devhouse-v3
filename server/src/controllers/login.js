import accountService from "services/login";

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
const loginAdmin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const account = await accountService.loginAdmin(email, password);
    if (account) {
      res.status(200).send({
        message: "Success",
      });
    } else {
      res.status(401).send({
        message: "Failed",
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({
      message: "Failed",
    });
  }
};

export default {
  loginAdmin,
};
