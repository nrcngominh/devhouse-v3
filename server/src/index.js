import mongoose from "mongoose";
import swaggerUi from "swagger-ui-express";
import yaml from "yamljs";
import app from "app";

const main = async () => {
  // Connect to MongoDB
  const mongoUri = process.env.MONGO_URI;
  try {
    await mongoose.connect(mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Connected to MongoDB");
  } catch (error) {
    console.log(error);
  }

  // Setup swagger
  if (process.env.NODE_ENV !== "production") {
    const swaggerDocument = yaml.load("./api-docs/swagger.yml");
    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }

  // Start server
  const server = app.listen(process.env.PORT || 3000, () => {
    console.log("Server is running on port", server.address().port);
  });
};

// Excute main function
main();
