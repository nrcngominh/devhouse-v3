import express from "express";
import accountController from "controllers/login";

const router = express.Router();

router.post("/login", accountController.loginAdmin);

export default router;
