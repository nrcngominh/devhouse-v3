import mongoose from "mongoose";

const accountSchema = mongoose.Schema({
  type: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String },
});

const accountModel = mongoose.model("Account", accountSchema, "accounts");

export default accountModel;
