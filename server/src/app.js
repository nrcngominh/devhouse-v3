import express from "express";
import cors from "cors";
import baseRouter from "routers";

// Create server
const app = express();

// Setup middleware
app.use(cors());
app.use(express.json());

// Setup router
app.use("/api", baseRouter);

export default app;
