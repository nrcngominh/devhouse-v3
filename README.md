# DevHouse

## Introduction

House for developers.

## Prerequisites

- `docker 19.03.0+`
- `docker-compose 1.26.0+`

## Quick start

Excute `scripts/start.sh` to start containers.
