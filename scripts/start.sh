#!/usr/bin/env bash

# Change directory to root
cd "$(dirname "$0")/.."

# Copy env file
cp .env.example .env

# Start containers with clean build
export COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1
docker-compose down -v --remove-orphans
docker-compose up -d --build
