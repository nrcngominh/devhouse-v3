import styled from "styled-components";
import { breakpoints } from "styles/breakpoints";

const Wrapper = styled.div`
  margin: auto;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  padding: 0.5rem;
  @media screen and (min-width: ${breakpoints.el}) {
    max-width: ${breakpoints.el};
  }
`;

export default Wrapper;
