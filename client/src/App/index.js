import React from "react";
import { BrowserRouter } from "react-router-dom";
import "normalize.css";
import "./global.css";
import AppRouter from "./router";
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <AppRouter />
      </BrowserRouter>
    </Provider>
  );
};

export default App;
