import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "modules/HomePage";
import LoginPage from "modules/LoginPage";
import ArcadePage from "modules/ArcadePage";

const Router = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route exact path="/login" component={LoginPage} />
      <Route exact path="/arcade" component={ArcadePage} />
    </Switch>
  );
};

export default Router;
