import { combineReducers } from "redux";
import { navigator } from "modules/PrimaryLayout/reducers";

export default combineReducers({
  navigator,
});
