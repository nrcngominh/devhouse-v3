import React from "react";
import Layout from "modules/PrimaryLayout";

const ArcadePage = (props) => {
  return <Layout tab="ARCADE">Arcade</Layout>;
};

export default ArcadePage;
