import React from "react";
import Layout from "modules/PrimaryLayout";

const HomePage = (props) => {
  return <Layout tab="HOME">Home</Layout>;
};

export default HomePage;
