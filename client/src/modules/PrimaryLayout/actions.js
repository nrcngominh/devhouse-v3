export const actionTypes = {
  NAVIGATE: "NAVIGATE",
};

export const navigateAction = (payload) => {
  return {
    type: actionTypes.NAVIGATE,
    payload,
  };
};
