import React from "react";
import styled from "styled-components";
import NavBarLink from "./NavBarLink";
import { tabs } from "./tabs";

const NavBar = styled.nav`
  display: flex;
  align-items: center;
  ul {
    li {
      display: inline-block;
      margin-left: 2rem;
    }
  }
`;

export default (props) => {
  return (
    <NavBar className={props.className}>
      <ul>
        <li>
          <NavBarLink text="TRANG CHỦ" to="/" tab={tabs.HOME} />
        </li>
        <li>
          <NavBarLink text="LUYỆN TẬP" to="/arcade" tab={tabs.ARCADE} />
        </li>
        <li>
          <NavBarLink text="ĐĂNG NHẬP" to="/login" tab={tabs.LOGIN} />
        </li>
      </ul>
    </NavBar>
  );
};
