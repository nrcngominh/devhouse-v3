import { actionTypes } from "./actions";

const navigator = (state = null, action) => {
  if (action.type === actionTypes.NAVIGATE) {
    return action.payload;
  }
  return state;
};

export { navigator };
