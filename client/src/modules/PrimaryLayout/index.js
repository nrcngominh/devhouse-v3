import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import Header from "./Header";
import Footer from "./Footer";
import { navigateAction } from "./actions";

const headerHeight = "3rem";

const FixedHeightHeader = styled(Header)`
  height: ${headerHeight};
`;

const Main = styled.main`
  background: #ccc;
  min-height: calc(100vh - ${headerHeight});
`;

export default (props) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(navigateAction(props.tab));
  });

  return (
    <>
      <FixedHeightHeader />
      <Main>{props.children}</Main>
      <Footer />
    </>
  );
};
