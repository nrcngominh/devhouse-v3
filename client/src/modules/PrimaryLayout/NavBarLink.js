import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { navigateAction } from "./actions";

const StyledLink = styled.div`
  padding: 0.2rem 0;
  border-bottom: ${(props) => (props.active ? "1px solid black" : "none")};
`;

export default (props) => {
  const dispatch = useDispatch();
  const currentTab = useSelector((state) => state.navigator);

  return (
    <Link to={props.to} onClick={() => dispatch(navigateAction(props.tab))}>
      <StyledLink active={props.tab === currentTab}>{props.text}</StyledLink>
    </Link>
  );
};
