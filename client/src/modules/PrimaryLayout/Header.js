import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import NavBar from "./NavBar";
import logo from "assets/logo.jpg";
import Wrapper from "components/ResponsiveWrapper";
import { fonts } from "styles/fonts";

const Logo = styled(Link)`
  width: auto;
  height: 100%;
  > img {
    width: auto;
    max-height: 100%;
  }
`;

const FlexContainer = styled(Wrapper)`
  display: flex;
  justify-content: space-between;
`;

const Header = styled.header`
  font-family: ${fonts.design};
  font-size: 1.1rem;
`;

export default (props) => {
  return (
    <Header className={props.className}>
      <FlexContainer>
        <Logo to="/">
          <img src={logo} alt="logo" />
        </Logo>
        <NavBar />
      </FlexContainer>
    </Header>
  );
};
