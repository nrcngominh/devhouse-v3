import React from "react";
import styled from "styled-components";
import Wrapper from "components/ResponsiveWrapper";

const Footer = styled.footer``;

export default (props) => {
  return (
    <Footer className={props.className}>
      <Wrapper>Footer</Wrapper>
    </Footer>
  );
};
