import React from "react";
import Layout from "modules/PrimaryLayout";

const LoginPage = (props) => {
  return <Layout tab="LOGIN">Login</Layout>;
};

export default LoginPage;
